; Brenta Brown Bears
; This model is authored by Sara Marini and Mick Neumann <sara.marini@stud.leuphana.de> <mick.neumann@stud.leuphana.de>
; Copyright 2024 CC BY-SA 4.0 Sara Marini and Mick Neumann
; The program was coded as an examination element for the class "Ecosystem Modeling" by Carsten Lemmen, Leuphana University, Summer Term 2024
; The program depicts the evolution of the brown bear population in Trentino, Italy, with a specific focus on human-bear interactions.
; See Info tab for further information.



;---------------------------------------------------------------------------------
;                     1. Extensions, Globals & Variables
;---------------------------------------------------------------------------------
__includes [ "NetLogoBears_calendar.nls" ]

extensions [ gis ]

globals [
  ;Trentino-Map
  forest-patches
  settlement-patches
  year
  month
  day
  doy
  days-in-months
]

breed [ bears bear ]
breed [ food a-food ]
breed [ bees bee ] ; makes sense to have them as patch, aka beehives?
breed [ sheep a-sheep ]
breed [ humans human ]

patches-own [ countdown land-use protectedarea? ]    ; countdown from the sheep-wolves-grass model (not sure if needed here too, I think it was to regenerate)
turtles-own [speed]

bears-own [
  age
  confidence
  injured?
  on-food?
  birth-tick
  pregnant-tick
  has-cubs?
  gender
]

humans-own [fear bear-education]






;--------------------------------------------------------------------------------
;                      2. Setup (Background, Turtles, Calender)
;--------------------------------------------------------------------------------
to setup
  reset-ticks
  clear-all
  setup-background
  setup-turtles
  setup-calendar
end



; --------------------------
to setup-background

; ------- Background using Map -----
  Import-pcolors "BearCity.png"

  ask patches [
    if pcolor = 64.3 [
      ifelse random-float 1.0 < 0.9 [
      set pcolor green
    ] [
      set pcolor red
    ]
      set land-use "forest"
    ]
  ]

  ask patches [
    if pcolor = 2.4 [
      set pcolor grey - 2
      set land-use "settlement"
    ]
  ]

  ask patches [
    if pcolor = 34.5 [
      set pcolor grey - 1
      set land-use "road"
    ]
  ]

end



; ---------------------------
to setup-turtles
  reset-ticks
  set-default-shape bears "bear"
  set-default-shape food "plant"
  set-default-shape bees "bee"
  set-default-shape humans "person"
  set-default-shape sheep "sheep"

  create-sheep 10 [
    set size 3
    set color white
    set heading 0
    move-to one-of patches with [ land-use = "settlement" ]
  ]

  create-bears initial-bears [
    set size 5
    set color brown
    set heading 0
    setxy random-xcor random-ycor
    set speed 0.001
    set confidence 0 ; probably we should say something like the confidence of the parents minus a certain value?
    set has-cubs? FALSE
    set injured? FALSE
    set age 3
    set birth-tick ticks
    set pregnant-tick 0
    set gender one-of ["male" "female"]
    move-to one-of patches with [ land-use = "forest" ]

  ask bears [
    if [pcolor] of patch-here = red  ; is bear on a red patch?
    [ set on-food? TRUE ]
  ]
  ]

end


;--------------------------------------------------------------------------------
;                      3. Go
;--------------------------------------------------------------------------------

to go

  advance-calendar
  tick
  if not any? turtles [ stop ]        ; simulation stops when all turltes are dead
;  define-forest
;  define-settlement

  ask bears [ go-bears ]
;  ask bears [ eat-food ]

  increment-age
  reproduce-bears

;  expand-village

end

;------ porcheria che non funziona
;turtles-own[
;  village-radius
;]
;to expand village
;  ask turtles[
;    ask patches in-radius pollution-radius [
;      set pcolor green
;    ]
;    if pollution-radius < 5 [
;      set pollution-radius pollution-radius + 0.1
;    ]
;  ]
;  tick
;end
;
;ask turtles[
;    ask patches in-radius pollution-radius [
;      set pcolor green
;    ]
;    if pollution-radius < 5 [
;      set pollution-radius pollution-radius + 0.1
;    ]
;  ]
;  tick
;end
; -----


; -------------- Increment age ---------------
to increment-age
  ask bears [
    let age-in-ticks ticks - birth-tick
    if (age-in-ticks > 0) and (age-in-ticks mod 365 = 0) [
      set age (age + 1)
    ]
  ]
end



;--------------- Reproduction -------------------
to reproduce-bears
  ; Check if it's the 1st of May and bears don't currently have cubs and male bears exist
  if any? bears with [gender = "male"] and day = 1 and month = 5 [
    ask bears [
      if age > 5 and gender = "female" and not has-cubs? and random-float 1.0 < (bear-reproductionrate) [
        set has-cubs? true ; Bear has cubs
        set pregnant-tick ticks ; Record the tick when the bear has cubs
      ]

      ; Check if the bear has cubs and the age is at least 3 years (3 * 365 ticks) + 1 so the newly "hatched" cubs dont get pregnant immediately
   if has-cubs? and (ticks - pregnant-tick = 3 * 365 + 1 ) [
        hatch-bears 1 [
          set shape "bear"
          set size 2
          set color brown
          set heading 0
          set confidence 0
          set has-cubs? false ; New bear doesn't have cubs
          set age 3 ; New bear starts at age 3 (because they stay 3 years with their mother right?)
          set birth-tick ticks ; Record birth tick for new bear
          set pregnant-tick 0
          set gender one-of ["male" "female"]
        ]
        set has-cubs? false ; Parent bear no longer has cubs
        set pregnant-tick 0
      ]
    ]
  ]

   ask bears[
    ifelse has-cubs?
    [ set color pink ]
    [ set color brown ]
  ]

;   set energy (energy / 2)      ; divide energy between parent and offspring (from the wolf sheep model, if we want to include energy in our model)
end




;---------------- Movement ----------------------
to go-bears
  ask bears [
    if  random-float 1.0 < 0.99  and [ land-use ] of one-of neighbors = "forest" [
      move-to one-of neighbors with [ land-use = "forest" ] ]
    if  random-float 1.0 > 0.99  and [ land-use ] of one-of neighbors = "settlement" [
      move-to one-of neighbors with [land-use = "settlement" ] ]
;    if land-use = "settelement"  > 1 tick [ die ] ;  bears stay more than one day in a village it gets shot
;    ; maybe we need a better command for this, one day is already quite long I think, I don't know though. And the thing is also, how do we let the variable "confidence rise?
;    ; If we use the parameter "bear was in a settlement once" then we need to see how long? If they stay for more than one tick are shot (or have to be removed maybe? I like this one better)
;    if land-use = "settelement"  > 1 tick [
;      move-to one-of patches with [ land-use = "forest"] ; bear is relocated
;      set confidence confidence + 1
;    ]
  ]
 end




; (from sheep wolf)
to eat-sheep  ; bear procedure
  let prey one-of sheep-here                    ; grab a random sheep
  if prey != nobody  [                          ; did we get one? if so,
    ask prey [ die ]                            ; kill it, and...
;    set energy energy + wolf-gain-from-food     ; get energy from eating
  ]
end






;;--------------------------------------------------------------------------------
;;                      Bears control
;;--------------------------------------------------------------------------------

to control-mortality
  ask bears [
    if confidence = 5 [ die ]
  ]

end






;; ------- To dos ---------------------------------
;
;
;
;to go
;; DONE bears get older ;https://stackoverflow.com/questions/29997530/setting-age-on-turtles
;; DONE bears reproduce
;; DONE bears die
;;      bears movement
;;      bears nutrition
;;
;;      bears interaction with ecosystem / why are they neded in mountains ecosystems
;;      bear interaction with bees
;;      bear interaction with domestic animals
;;      bear interaction with excursionists/people in woods (attacks)
;;      bear education and fear
;          if attack happens [
;          set fear fear + 1]
;
;;      people --> woods exploitation (-> habitat loss)
;end
;
;
;
;
;;------------ End todos ----




@#$#@#$#@
GRAPHICS-WINDOW
246
15
853
623
-1
-1
1.997
1
10
1
1
1
0
1
1
1
0
299
0
299
0
0
1
ticks
30.0

BUTTON
29
23
122
78
NIL
setup\n
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
135
22
224
78
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
32
125
229
158
ElectricalFences
ElectricalFences
0
100
52.0
1
1
NIL
HORIZONTAL

MONITOR
860
18
970
63
Number of Bears
count bears
17
1
11

BUTTON
138
86
216
119
go-once
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

MONITOR
862
74
919
119
NIL
day
17
1
11

MONITOR
924
74
981
119
NIL
month
17
1
11

MONITOR
863
132
920
177
NIL
doy
17
1
11

MONITOR
985
75
1042
120
NIL
year
17
1
11

MONITOR
925
131
991
176
NIL
leap-year
17
1
11

SLIDER
30
165
229
198
bear-reproductionrate
bear-reproductionrate
0
1
0.14
0.01
1
NIL
HORIZONTAL

MONITOR
865
202
1107
247
NIL
count bears with [has-cubs? = TRUE]
17
1
11

MONITOR
871
292
1190
337
NIL
count patches with [pcolor = green or pcolor = red]
17
1
11

MONITOR
871
347
1076
392
NIL
count patches with [pcolor = red]
17
1
11

PLOT
869
476
1069
626
Bears
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot count bears"

SLIDER
30
206
202
239
initial-bears
initial-bears
2
300
165.0
1
1
NIL
HORIZONTAL

MONITOR
977
18
1052
63
Male Bears
count bears with [gender = \"male\"]
17
1
11

MONITOR
1065
18
1150
63
Female Bears
count bears with [gender = \"female\"]
17
1
11

@#$#@#$#@
## WHAT IS IT?

(a general understanding of what the model is trying to show or explain)

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

bear
true
0
Polygon -7500403 true true 25 141 16 122 18 111 33 116 40 123 67 111 79 110 98 104 107 97 118 90 131 97 138 109 154 111 184 114 200 119 218 124 228 127 233 128 256 167
Polygon -7500403 true true 27 140 25 131 16 139 13 147 17 156 13 165 10 169 25 180 41 173 47 183 47 193 45 201 38 220 34 230 26 235 10 254 12 259 25 256 40 242 49 237 58 235 91 209 102 193 118 199 122 220 123 232 124 242 123 254 117 261 103 266 117 269 135 266 148 266 151 252 149 240 147 226 147 219 146 209 147 203 165 204 183 210 177 221 168 232 167 244 164 252 159 259 145 259 152 269 171 270 190 269 194 254 201 246 212 234 216 223 217 217 220 217 233 231 234 238 237 245 242 256 255 270 243 280 262 280 277 278 281 274 284 271 284 260 278 243 272 223 269 199 260 188 259 176 255 163 55 134 25 132 45 138 29 136 24 135 22 135 21 134 127 128 89 151 74 150 110 135
Polygon -7500403 true true 77 134 119 145 125 166 70 164 73 145 90 134 105 156

bee
true
0
Polygon -1184463 true false 152 149 77 163 67 195 67 211 74 234 85 252 100 264 116 276 134 286 151 300 167 285 182 278 206 260 220 242 226 218 226 195 222 166
Polygon -16777216 true false 150 149 128 151 114 151 98 145 80 122 80 103 81 83 95 67 117 58 141 54 151 53 177 55 195 66 207 82 211 94 211 116 204 139 189 149 171 152
Polygon -7500403 true true 151 54 119 59 96 60 81 50 78 39 87 25 103 18 115 23 121 13 150 1 180 14 189 23 197 17 210 19 222 30 222 44 212 57 192 58
Polygon -16777216 true false 70 185 74 171 223 172 224 186
Polygon -16777216 true false 67 211 71 226 224 226 225 211 67 211
Polygon -16777216 true false 91 257 106 269 195 269 211 255
Line -1 false 144 100 70 87
Line -1 false 70 87 45 87
Line -1 false 45 86 26 97
Line -1 false 26 96 22 115
Line -1 false 22 115 25 130
Line -1 false 26 131 37 141
Line -1 false 37 141 55 144
Line -1 false 55 143 143 101
Line -1 false 141 100 227 138
Line -1 false 227 138 241 137
Line -1 false 241 137 249 129
Line -1 false 249 129 254 110
Line -1 false 253 108 248 97
Line -1 false 249 95 235 82
Line -1 false 235 82 144 100

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.4.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
