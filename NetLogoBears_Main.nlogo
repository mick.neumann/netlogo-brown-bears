; "Brown Bear reintroduction in the Brenta region, northern Italy:
;  modeling human-bear interactions around the village of Caldes"
; This model is authored by Sara Marini and Mick Neumann <sara.marini@stud.leuphana.de> <mick.neumann@stud.leuphana.de>
; It is licensed under CC-BY 4.0
; © 2024 by Sara Marini and Mick Neumann
; The program was coded as an examination element for the class "Ecosystem Modeling" by Carsten Lemmen, Leuphana University, Summer Term 2024
; The program depicts the evolution of the brown bear population in Trentino, Italy, with a specific focus on human-bear interactions.
; See Info tab for further information.



;---------------------------------------------------------------------------------
;        1. Included files, extensions, globals, breeds & agents-owed variables
;---------------------------------------------------------------------------------

__includes [
  "NLB_Calendar.nls"
  "NLB_BearBehaviour.nls"
  "NLB_HumanBehaviour.nls"
  "NLB_AttackDynamic.nls"
  "NLB_Animations.nls"
  "NLB_ManagementMeasures.nls"
]

extensions [ bitmap ]

globals [
  ; calendar
  year
  month
  day
  doy
  days-in-months

  ; bear-human interaction
  encounter-count
  attack-count
  fear
  dissatisfaction

  ; attack dynamic
  attack-risk-score
  attack-happened?
  attack-spot
  last-attack-tick

  ; poaching
  poach-happened?
  poach-spot
  poach-count

  ; bear behavior
  bear-reproduction-rate
  patch-to-go-bears
  eaten-sheep-count
  beehive-destroyed-count

  ; management measures
  button-press-count
  bear-education
  bear-education-count
  bear-education-tick
  cull-count
]

breed [ bears bear ]
breed [ sheep a-sheep ]
breed [ humans human ]
breed [ beehives beehive ]
breed [ attack-animations attack-animation ]
breed [ attack-spot-markers attack-spot-marker ]
breed [ poach-animations poach-animation ]
breed [ poach-spot-markers poach-spot-marker ]

patches-own [
  land-use
  food?
  attack-tick-spot
  poach-tick
]

bears-own [
  age
  gender
  confidence
  injured?
  has-cubs?
  flagged?
  birth-tick
  pregnant-tick
  injured-tick
]

humans-own [
  in-group?
  with-dog?
]

beehives-own [
  fenced?
  destroyed?
  beehive-destroyed-tick
]




;--------------------------------------------------------------------------------
;           2. Startup procedure
;--------------------------------------------------------------------------------
to startup
 license-message
 setup-background
 setup-turtles
 setup-bears
 setup-calendar
 setup-plots
end


; ------- License message --------
to license-message
  print "This model is authored by Sara Marini and Mick Neumann" ; <sara.marini@stud.leuphana.de> <mick.neumann@stud.leuphana.de>
  print "Copyright 2024 CC-BY 4.0 Sara Marini and Mick Neumann"
  print "The model was coded as an examination element for the class 'Ecosystem Modeling'"
  print "taught by Dr. Carsten Lemmen at Leuphana University, Summer Term 2024"
  print "The program models the evolution of the brown bear population in the Brenta region,"
  print "Italy, after its reintroduction through the 'Life Ursus' project back in 2002,"
  print "with a specific focus on human-bear interactions around the village of Caldes."
  print "See Info tab for further information."
end



;------- Background map & legend ----

to setup-background
  Import-pcolors "NetLogoBears_pics/VillageMap.png"        ; Map of Caldes has been drawn starting from GIS data

  ask patches [                                            ; Assign one of four land-uses (forest, settlement road,
    if pcolor = 56.3 [                                     ; livestock-area) according to the patch' color and assign
      set land-use "forest"                                ; food to a certain percentage of patches depending on land-use
      ifelse random-float 1.0 < 0.9 [
        set pcolor green + 1
        set food? FALSE
      ] [
        set pcolor green
        set food? TRUE
      ]
    ]
    if pcolor = 25.4  [
      set land-use "settlement"
      ifelse random-float 1.0 < 0.10 [
        set food? TRUE
      ] [
        set food? FALSE
      ]
    ]
    if pcolor = 4 or pcolor = 36 [
      set pcolor gray - 1
      set land-use "road"
      ifelse random-float 1.0 < 0.10 [
        set food? TRUE
      ] [
        set food? FALSE
      ]
    ]
   if pcolor = 47.3 [
      set land-use "livestock-area"
      ifelse random-float 1.0 < 0.10 [
        set food? TRUE
      ] [
        set food? FALSE
      ]
    ]
  ]

;-Legend
  let legend bitmap:import "NetLogoBears_pics/legend.png"
  bitmap:copy-to-drawing legend 0 480

  ask patches [                                            ; Change the land-use of patches behind the legend,
    if pxcor < 55 and pycor < 54 [                         ; so that turtles do not walk on it. The green box is
    set pcolor green + 1                                   ; slightly bigger than the actual legend to avoid turtles
    set land-use "legend"                                  ; being visually with half of their bodies on the legend
    ]                                                      ; when their core is exactly on the border of the box.
  ]
end



;------- Creating turtles and, separately, bears ------
to setup-turtles
  reset-ticks
  set-default-shape beehives "bee"
  set-default-shape sheep "sheep"
  set-default-shape humans "person"
  set-default-shape bears "bear"


  create-beehives 10 [
    set size 9
    set color grey
    move-to one-of patches with [ land-use = "forest" ]
    set heading 0
    set destroyed? FALSE
    set fenced? FALSE
    if fenced? [
      set shape "bee-fenced"
    ]
  ]

  create-sheep 10 [
    set size 5
    set color white
    set heading 0
    move-to one-of patches with [ land-use = "livestock-area" ]
  ]

  create-humans initial-humans [                        ; initial-humans is set through a slider on the interface
    set size 5
    set color blue - 3
    set heading 0
    set in-group? one-of [TRUE FALSE]
    set with-dog? one-of [TRUE FALSE]
    set bear-education bear-education
    set fear initial-fear                               ; initial-fear is set through a slider on the interface
    move-to one-of patches with [ land-use = "road" ]
  ]
end

; Bears are created separately to avoid code repetition in the "setup-reintroduction" procedure below. There, in fact,
; all other turtles can be setup as in this "setup-turtles" procedure, while bears need some specific parameters instead.
to setup-bears
  create-bears initial-bears [
    set size 8
    set color brown - 1
    set heading 0
    set confidence 0
    set has-cubs? FALSE
    set injured? FALSE
    set flagged? FALSE
    set age random 18 + 3   ; set age between 3 and 20
    set birth-tick ticks
    set pregnant-tick 0
    set gender one-of ["male" "female"]
    set flagged? FALSE
    move-to one-of patches with [ land-use = "forest" ]
  ]
end




;--------------------------------------------------------------------------------
;                   3. Buttons "Setup" and "Setup-reintroduction"
;--------------------------------------------------------------------------------

; ------- Random setup --------------------------------
; "Setup" gives the user the possibility to set up the model again and again and to run
; different simulations with different parameters.
to setup
  clear-turtles
  clear-globals
  clear-all-plots
  restore-background
  setup-turtles
  setup-bears
  setup-calendar
  reset-ticks
end


; ----- Setup reintroduction scenario Life Ursus -------
; "Setup-reintroduction", automatically sets the model's parameters as to reproduce the conditions of the
; area as the rewilding project "Life Ursus", reintroducing brown bears in the region, was completed in 2002.

to setup-reintroduction

  ; general settings can be set-up as in the "setup" procedure
  clear-turtles
  clear-globals
  clear-all-plots
  restore-background
  setup-calendar
  reset-ticks

  ; bear-turtles are created according to the characteristics of the bears released back in 1999-2002
  create-bears 9 [
    set size 8
    set color brown - 1
    set heading 0
    set confidence 0
    set has-cubs? FALSE
    set injured? FALSE
    set flagged? FALSE
    set pregnant-tick 0
    set age random 3 + 3                    ;  Bears selected for the reintroduction were all aged 3 - 6
    set birth-tick ticks
    if who < 7 [ set gender "female" ]      ; 7 released individuals were females
    if who >= 7 [ set gender "male" ]       ; the other two individuals were males
    if who < 2 [
      set age 6
      set has-cubs? TRUE                    ; Two bears were pregnant in 2002 and gave birth in 2002 and 2003
      set color pink
    ]
    move-to one-of patches with [           ; Bears were released in forest spots far away from human presence
      land-use = "forest" and
      not any? patches in-radius 35 with [
        land-use = "settlement" or
        land-use = "road" or
        land-use = "livestock-area"]
    ]
  ]

  ; setup all other turtles, set the year and the sliders / variables to the right values
  set year 2002                             ; Last bear released in 2002, then the rewilding project "Life Ursus" was officially closed
  set initial-bears 9
  set initial-bear-reproduction-rate 0.9    ; Assumed to be high because released bears were young, healthy and reproduced regularly after being released
  set initial-humans 100                    ; estimated number of people moving daily around the village (1/3 of Caldes' population)
  set initial-fear 1                        ; innate basic fear of big carnivores; most of the inhabitants were not concerned about the reintroduction of the brown bear though
  set bear-education 0                      ; no information was spread about how to behave in case of encounters with bears
  setup-turtles
end



; ----- Restore background map -------
; For the sake of efficiency, "setup" and "setup-reintroduction" do not load the background picture again (as
; the startup procedure) but only restore the patches that have been changed during the previous simulation.

to restore-background
  ask patches with [ plabel = "Attack spot" or plabel = "Poach"] [
    set plabel ""
  ]
end




;--------------------------------------------------------------------------------
;                      4. Button "Go"
;--------------------------------------------------------------------------------

to go
  advance-calendar                                             ; included file "NetLogoBears_Calendar.nls"
  tick
  if not any? bears [
    print "The model has stopped because all bears are dead."
    stop
  ]
  go-bears                                                     ; included file "NLB_BearBehaviour.nls"
  go-humans                                                    ; included file "NLB_HumanBehaviour.nls"
  run-attack-dynamic                                           ; included file "NLB_AttackDynamic.nls"
  handle-animations                                            ; included file "NLB_animations.nls"
  regulate-fear
  regulate-dissatisfaction
  restore-beehives
  manage-coexistence                                           ; included file "NLB_ManagementMeasures.nls"
end






;------- Fear naturally decreases as time passes and no attacks happen --------
to regulate-fear
  let decrease-factor 4 ^ (1 / (365 * 4))
  set fear fear / decrease-factor
end

; Note about the function
; By dividing repeatedly by the same factor, you end up with a decreasing
; exponential that goes to zero for ticks going to infinity.
; The decrease rate has been calculated so that fear would be reduced
; to 1/4 of its original value after 4 consecutive years of no events letting
; fear rise (bear encounters, bears' attacks to sheep, beehives or humans).
; This arbitrary value is based on an estimate of the time that people
; reasonably need in real life to "erase the memory" of an attack. In the model
; in fact, each attack is worth + 4 fear points; after 4 years fear level
; would be reduced to 1/4 of this value, namely to 1 point, which equals
; the increase of fear due to an encounter that did not turn into an attack.


to regulate-dissatisfaction
  if fear > 9 and dissatisfaction < 15  [
    set dissatisfaction dissatisfaction + 0.01
  ]
end


; ------ Beehives are built again 3 months after bears' attack
to restore-beehives
  ask beehives with [ destroyed? ] [
    if ticks =  beehive-destroyed-tick + 90 [
      set destroyed? FALSE
    ]
  ]
end
@#$#@#$#@
GRAPHICS-WINDOW
243
54
1020
640
-1
-1
1.9233333333333333
1
10
1
1
1
0
0
0
1
0
399
0
299
1
1
1
days
30.0

BUTTON
18
462
75
495
Setup
setup\n
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
156
130
225
167
Go
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

MONITOR
1088
279
1238
320
Total bears (males, females)
word count bears \" (\" count bears with [gender = \"male\"] \", \" count bears with [gender = \"female\"] \")\"
17
1
10

MONITOR
1298
10
1390
59
Date
word day\"/\"month\"/\"year
17
1
12

SLIDER
14
284
224
317
initial-bear-reproduction-rate
initial-bear-reproduction-rate
0
1
0.9
0.01
1
NIL
HORIZONTAL

PLOT
1027
65
1408
294
Bears: total (black) and flagged (red)
Days
Number of bears
0.0
10.0
0.0
10.0
true
false
";clear-plot" ""
PENS
"default" 1.0 0 -16777216 true "clear-plot \nplot initial-bears" "plot count bears"
"pen-1" 1.0 0 -5298144 true "" "plot count bears with [flagged?]"

SLIDER
14
249
225
282
initial-bears
initial-bears
2
300
40.0
1
1
NIL
HORIZONTAL

MONITOR
1241
279
1300
320
Mean age
mean [ age ] of bears
2
1
10

SLIDER
13
335
226
368
initial-humans
initial-humans
0
1000
100.0
1
1
NIL
HORIZONTAL

MONITOR
1271
328
1338
369
Encounters
encounter-count
17
1
10

MONITOR
1304
279
1391
320
Flagged bears
count bears with [flagged? = TRUE]
17
1
10

MONITOR
1341
328
1392
369
Attacks
attack-count
17
1
10

TEXTBOX
16
237
166
255
Bears
9
0.0
1

TEXTBOX
15
323
116
341
Humans
9
0.0
1

MONITOR
1202
551
1298
592
Dissatisfaction
precision dissatisfaction 5
5
1
10

MONITOR
1204
600
1297
641
Education level
bear-education
17
1
10

TEXTBOX
256
15
1007
48
Brown Bears: Human-Bear interactions around Caldes, Italy
26
0.0
1

MONITOR
1089
328
1195
369
Damaged Beehives
beehive-destroyed-count
17
1
10

BUTTON
78
462
167
495
Random seed
clear-turtles\nclear-globals\nclear-all-plots\nrandom-seed 1\nrestore-background\nsetup-turtles\nsetup-bears\nsetup-calendar\nreset-ticks
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

MONITOR
1100
551
1198
592
Fear
precision fear 3
17
1
10

SWITCH
20
538
134
571
cull-bears?
cull-bears?
0
1
-1000

BUTTON
18
130
151
167
Setup reintroduction
setup-reintroduction
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
20
608
176
641
on-off fence beehives
fence-beehives
NIL
1
T
TURTLE
NIL
NIL
NIL
NIL
1

TEXTBOX
21
58
235
135
By pressing \"setup-reintroduction\", the model is initialized as to reproduce the conditions of the ecosystem back in 2002, as the rewilding project \"Life Ursus\" was completed. \nThen press \"go\" to see the historical evolution of the system until 2024 and beyond
9
0.0
1

TEXTBOX
18
183
235
228
Alternatively, you can set the following sliders to the desired values, press either \"Setup\" or \"Random seed\" to initialize the model to those values and then \"Go\" to start the simulation 
9
0.0
1

TEXTBOX
17
424
218
457
Use \"Setup\" if you prefer to see a new model configuration every time or \"Random Seed\" to set the model to the same initial scenario
9
0.0
1

SLIDER
13
371
226
404
initial-fear
initial-fear
1
15
10.0
1
1
NIL
HORIZONTAL

TEXTBOX
19
517
169
535
Management measures
13
0.0
1

SWITCH
20
573
134
606
educate?
educate?
0
1
-1000

MONITOR
1101
600
1198
641
N. of campaigns
bear-education-count
17
1
10

PLOT
1028
384
1386
543
Fear over time
Days
Fear
0.0
10.0
0.0
10.0
true
false
"clear-plot\nset fear initial-fear \nplot fear" ""
PENS
"default" 1.0 0 -16777216 true "" "plot fear"

TEXTBOX
146
543
229
609
You can decide which of the 3 management measures are implemented
9
0.0
1

MONITOR
1197
328
1269
369
Killed sheep
eaten-sheep-count
0
1
10

MONITOR
1303
600
1385
641
Culled bears
cull-count
17
1
10

MONITOR
1302
551
1386
592
Poach events
poach-count
17
1
10

TEXTBOX
23
10
208
61
Here you can play around with the model settings
14
25.0
1

TEXTBOX
1033
10
1244
61
On this side, you can observe the output of the simulation
14
25.0
1

TEXTBOX
1028
560
1080
590
People's reactions
9
0.0
1

TEXTBOX
1027
608
1113
651
Management measures
9
0.0
1

TEXTBOX
1033
331
1090
370
Bears' damage to humans
9
0.0
1

TEXTBOX
1031
283
1083
316
Bear population specifics
9
0.0
1

BUTTON
170
462
225
495
Go
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

@#$#@#$#@
## WHAT IS IT?

This model simulates human-bear interactions around the settlement of Caldes, Trentino, Italy. The region (Trentino) was the heart of a rewilding project completed in 2002 (_Life Ursus_) and oftentimes the scenario of conflicts between local inhabitants and the big carnivore. In the spring of 2023 one bear-human encounter in the woods around Caldes resulted in a lethal attack and the topic of human-bear coexistence strongly returned at the center of political and scientific attention. 
This model focuses exactly on the dynamics of interaction and interference between the brown bear population and human activities. It aims to understand the population dynamics and the chances of attacks happening in the region taking into consideration human factors (e.g. number of locals, their knowledge about bears), characteristics of brown bears in the region (e.g. population size, their increasing confidence in approaching settlements), as well as different management approaches to improve the coexistence of the two species (e.g. culling, promoting education about bears).

## HOW IT WORKS


#### Time scale 

In the model, each tick represents one day, giving the user a snapshot of the location of the agents each day in the region around the village Caldes. 

#### Patches (Landscape)

The background map represents the settlement of Caldes (orange) and its surrounding woods. The map has been drawn on the basis of the publicly available [OpenStreetMap data](https://www.openstreetmap.org/search?query=caldes#map=15/46.37031/10.93322&layers=P).The landscape is divided into 4 different land-use types: forest, settlements, roads and livestock areas. This is the main chracteristic influencing the movement of the agents in the model and therefore the likelihood of interactions between them.

#### Turtles

The model includes several types of turtles: bears, humans, sheep (representing livestock), and beehives. Each turtle follows specific rules:

- Bears move through the landscape, reproduce, and potentially interact with humans. Their movement is modelled so that they spend most of their time in nature, roaming around. Occasionally they step on roads, in a livestock area, close to a beehive or in the settlement. In winter, during hibernation, bears are moved to areas further away from roads and the settlement and will move overall a lot less.
In November, female bears have a chance of getting pregnant. Seven months later, they give birth and the property has-cubs? is set to TRUE. This means, that they are currently the mother to one to three cubs. These cubs don’t become independent turtles until the age of three. This is done for simplification and allows us to track mother bears with cubs, wich is relevant because they tend to be more aggressive if they are protecting their cubs. This is, among other factors, a factor influencing the chance of an attack in case of a human-bear encounter.

- Humans represent people who are currently out from their homes. They mostly move on roads, with sporadic walks in the forest. The frequency of their movement in nature is regulated through a balancing feedback loop: the higher the fear of bears among humans, the higher the likelihood that humans move on roads and the lower the movements in forests. This, in turn, decreases the likelihood of human-bear encounters and attacks, letting fear decrease again. Furthermore, humans retrieve to the village after each attack and slowly spread out again as time passes and fear decreases. Each human turtle can either represent a single individual or a group with or without a dog, as stored by the human-owned properties "in-group?" and "with-dog?" (relevant for the attack dynamic, see next point).

- Livestock and beehives: in rel life, these tend to be two of the most common target for bears. In the model, they therefore influencing bears' movement: bears tend to move towards them if they are close enough to notice them. There is one designated, representative, livestock area, harbouring sheep. The sheep can be eaten by the bears.
The beehives are spread randomly across the map and can be approached and destroyed by bears. When a Bear is in a close range to a beehive it will be moved there next, leading to the destruction of the beehive. That only happens, when there is no fence around the beehive.

#### Attack dynamic  
The precondition of an attack is a human-bear encounter. This happens whenever a human and a bear stand on the same patch. Then, the probability that the enounter ends up in an actual attack is calculated according to differeent variables characterizing the situation: has the bear been surprised on food? Are there any cubs to protect? Is the person walking alone or in a group? Is there any dog threatening the bear? Each property is assigned a value representing the increase in risk and, if the overall risk-score is above a certain threshold, the attack count goes up, you can see an animation in the view and the attack spot is marked with a red spot on the map.

## HOW TO USE IT

#### Default run
For getting a first feeling of the model, you can simply press GO. The model is already setup with some realistic parameters.

#### Adjusting the settings to your liking
If you want to change settings, do the following:
Adjust the sliders and switches to your liking
Press "Setup" or "Random seed" to  apply the changes 
Press GO to begin the simulation
NOTE: "Setup" will show a new view complying with the given values every time it is pressed; random seed will present the same configuration everytime until the sliders are set again to differnt values.


The parameters that you can adjust before starting any new simulations are the following:

- Sliders: You can adjust the initial number of bears and humans in the area as well as the reproduction rate regulating the flux of the bear population. There is no one reproduction rate that can be assumed for the area specifically, but a paper (spurce) suggests the reproduction rate for brown bears to generally range from 0.23 to 0.96.

- Switches and buttons: You can switch on and off two management strategies for the brown bear population:

1. The switch for cull-bears? will activate the strategic killing of bears that are considered particularly dangerous due to their previous attacks to humans or livestock (bears with flagged? = TRUE), or because they have often step foot in the village (bers with a high "confidence" score).

2. The button beehive fencing will put electric fenced around all beehives, so they cannot be attacked by bears.


#### "Reintroduction scenario" 
There is a button for setting up a specific scenario: "Setup reintroduction". This will set the model to to 2002, as the reintoduction project "Life Ursus" was completed, and recreate the ecosystems conditions back then: 9 individuals released (two males and seven females), high reproduction rate and low human fear of bears.


After pressing "go", the changes can be observed in different places on the interface.

- View: gives a qualitative impresion of what is happening

- Monitors: the upper monitor shows the date, the ones in first row some bear-specific parameters (total number of individuals, males, females, average age, flagged bears, namely bears that have been marked as potentially dangerus after attacks to animals or humans), the second row shows the damage created by ears to humans (number of destroyed beehives, killed sheep, bear-human encounters and attacks). Below, the secodn to last row shows ear level, dissa

- Plots: the first plot shows the development of the bear population and of the number of flagged bears. The second plot shows the evolution of fear in humans over time.

- Command center: prints messages whenever a bear moved attarcted by beehives or livestocks and shows the current reproduction rate of the bear population as soon as it is updated from year to year. This allows you to follow some relevant processes of the model that would otherwise remain invisible.


#### End of the model
The model stops when there are no more bears on the map.



## THINGS TO NOTICE

Have a look at the evolution of the bear population: how does it change over time according to differnt reproduction rates? What is the influence of diffenret management strategies? Notice in particular the different effects of culling and poaching: does culling prevent poaching?
Observe the interactions between bears and humans, e.g. how often and where encounters occur. How does the fear change over time?
Notice the feedback loop between the fear level among humans, how it affects their movement and how often attacks happen, influencing in turn the fear level.


## THINGS TO TRY

Try to play around with combinations of sliders and switches.
Vary the initial bear and human populations to see how it affects the simulation.
Increase or decrease the initial fear level to observe changes in human movement and encounter frequency. Toggle the fencing of beehives to study its effectiveness in reducing bear-related damage.

Try the "reintroduction" scenario that starts 2002 and stop the model in 2024: does the bear population develop in a realistic way according to present day data (around 90 bears, 4-6 attacks and many more encounters)? Let it run further to see how the bear population could potentially evolve in the area in the future.

The model also has three experiments already set up in the BehaviorSpace tool.


## EXTENDING THE MODEL

Modeling the occurrence of human-bear encounters and potential resulting attacks is a very hard task: it is a rare phenomenon depending on an extremely large number of interplaying variables. This model uses a representative environment and a highly simplified representation of bears and humans' behaviours to model the likelihood of humans and bears meeting each other and could be extended and refined in different ways.

First, both the human and bear behaviour and movements could be further improved. A first possibility would be to introduce more attractors influencing bear behavior, for example trash can or smells in general. Furthermore, in reality bears tend to move in such a way as to minimize the encounters with other bears (spatial avoidance) and humans, whenever possible. In this version of the model bears do not adjust to the presence of other bears and humans; extending the model with such a functionality could significantly improve the accuracy of the outcome. 
Human behaviour is also incomplete. In this model, in fact, people only move and shoot bears (legally through culling or illegally through poaching), but do not exert any indirect influence on them through landscape modification. In reality, activities like logging and livestock keeping are major factors putting the local population of bears under pressure, as they contribute to the reduction and fragmentation of the bears' habitat. Modeling these activities, focusing on the expansion of anthropic infrastructure over time, could improve the precision of the predictions for the future.

Another key point to increase the accuracy of the model would be implementing day-night cycles. The majority of the encounters and of the attacks, in fact, happens in the evening hours. As humans are usually more active during the day, bears, in fact, reacted to that and adapted to be more active in the night to avoid humans. Most of the critical hours for human-bear encounters are therefore at dawn. As we only create snapshots of each day, the movement of humans and bears cannot be tracked for every time of the day. A more advanced model would take these time differences into account and highlight the distribution of attacks depending on the time of the day (the increased risk in the evening hours could be tested out and the efficacy of the recommendation to stay home after dawn to reduce attacks could be quantified).


Finally the model could be extended by introducing more species, food as a limiting factor and an energy balance.
The environment is biologically very static: food is assumed to be present in abundance and the trophic interactions of brown bears with other wild species are not modeled at all. Introducing the variable of energy, considering the evolution of food in the area (potentially becoming a limiting factor in the growth of the bear population) and a few more species directly related o the bear would make the model more complicated yet complete, for instance sheding light on the long-term ecological consequences for the ecosystem in the case of a new extinction of the brown bear population in the region.



## NETLOGO FEATURES

Bitmap Extension: Utilized for importing and displaying images for the map and legend.
No unusual features are present in the code.


## RELATED MODELS

The model took some inspiration from the wolf-sheep predation model and made use of the draft of a calendar implementation by Dr. Carsten Lemmen.



## CREDITS, LICENSE AND REFERENCES

This model is authored by Sara Marini and Mick Neumann, as part of the "Ecosystem Modeling" class taugtht by Dr. Carsten Lemmen at Leuphana University.
It is licensed under [CC BY 4.0.](https://creativecommons.org/licenses/by/4.0/?ref=chooser-v1)
© 2004 by Sara Marini and Mick Neumann 

For more details, contact the authors at sara.marini@stud.leuphana.de and mick.neumann@stud.leuphana.de.

![CC BY 4.0 logo](https://s2.qwant.com/thumbr/88x31/6/7/f2e206d0b52e839960df9858a864ace437efc957c71f09cdf22aecb42c2926/th.jpg?u=https%3A%2F%2Ftse.mm.bing.net%2Fth%3Fid%3DOIP.n9jerNeFqzgA5ZFybusyWQAAAA%26pid%3DApi&q=0&b=1&p=0&a=0)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

bear
true
0
Polygon -7500403 true true 25 141 16 122 18 111 33 116 40 123 67 111 79 110 98 104 107 97 118 90 131 97 138 109 154 111 184 114 200 119 218 124 228 127 233 128 256 167
Polygon -7500403 true true 27 140 25 131 16 139 13 147 17 156 13 165 10 169 25 180 41 173 47 183 47 193 45 201 38 220 34 230 26 235 10 254 12 259 25 256 40 242 49 237 58 235 91 209 102 193 118 199 122 220 123 232 124 242 123 254 117 261 103 266 117 269 135 266 148 266 151 252 149 240 147 226 147 219 146 209 147 203 165 204 183 210 177 221 168 232 167 244 164 252 159 259 145 259 152 269 171 270 190 269 194 254 201 246 212 234 216 223 217 217 220 217 233 231 234 238 237 245 242 256 255 270 243 280 262 280 277 278 281 274 284 271 284 260 278 243 272 223 269 199 260 188 259 176 255 163 55 134 25 132 45 138 29 136 24 135 22 135 21 134 127 128 89 151 74 150 110 135
Polygon -7500403 true true 77 134 119 145 125 166 70 164 73 145 90 134 105 156

bee
true
0
Polygon -1184463 true false 152 149 77 163 67 195 67 211 74 234 85 252 100 264 116 276 134 286 151 300 167 285 182 278 206 260 220 242 226 218 226 195 222 166
Polygon -16777216 true false 150 149 128 151 114 151 98 145 80 122 80 103 81 83 95 67 117 58 141 54 151 53 177 55 195 66 207 82 211 94 211 116 204 139 189 149 171 152
Polygon -7500403 true true 151 54 119 59 96 60 81 50 78 39 87 25 103 18 115 23 121 13 150 1 180 14 189 23 197 17 210 19 222 30 222 44 212 57 192 58
Polygon -16777216 true false 70 185 74 171 223 172 224 186
Polygon -16777216 true false 67 211 71 226 224 226 225 211 67 211
Polygon -16777216 true false 91 257 106 269 195 269 211 255
Line -1 false 144 100 70 87
Line -1 false 70 87 45 87
Line -1 false 45 86 26 97
Line -1 false 26 96 22 115
Line -1 false 22 115 25 130
Line -1 false 26 131 37 141
Line -1 false 37 141 55 144
Line -1 false 55 143 143 101
Line -1 false 141 100 227 138
Line -1 false 227 138 241 137
Line -1 false 241 137 249 129
Line -1 false 249 129 254 110
Line -1 false 253 108 248 97
Line -1 false 249 95 235 82
Line -1 false 235 82 144 100

bee-destroyed
true
0
Polygon -1184463 true false 152 149 77 163 67 195 67 211 74 234 85 252 100 264 116 276 134 286 151 300 167 285 182 278 206 260 220 242 226 218 226 195 222 166
Polygon -16777216 true false 150 149 128 151 114 151 98 145 80 122 80 103 81 83 95 67 117 58 141 54 151 53 177 55 195 66 207 82 211 94 211 116 204 139 189 149 171 152
Polygon -7500403 true true 151 54 119 59 96 60 81 50 78 39 87 25 103 18 115 23 121 13 150 1 180 14 189 23 197 17 210 19 222 30 222 44 212 57 192 58
Polygon -16777216 true false 70 185 74 171 223 172 224 186
Polygon -16777216 true false 67 211 71 226 224 226 225 211 67 211
Polygon -16777216 true false 91 257 106 269 195 269 211 255
Line -1 false 144 100 70 87
Line -1 false 70 87 45 87
Line -1 false 45 86 26 97
Line -1 false 26 96 22 115
Line -1 false 22 115 25 130
Line -1 false 26 131 37 141
Line -1 false 37 141 55 144
Line -1 false 55 143 143 101
Line -1 false 141 100 227 138
Line -1 false 227 138 241 137
Line -1 false 241 137 249 129
Line -1 false 249 129 254 110
Line -1 false 253 108 248 97
Line -1 false 249 95 235 82
Line -1 false 235 82 144 100
Polygon -2674135 true false 45 60 30 75 135 150 60 240 75 255 150 165 240 240 255 225 165 150 240 60 225 45 150 135 45 60 60 75

bee-fenced
true
0
Polygon -1184463 true false 152 149 77 163 67 195 67 211 74 234 85 252 100 264 116 276 134 286 151 300 167 285 182 278 206 260 220 242 226 218 226 195 222 166
Polygon -16777216 true false 150 149 128 151 114 151 98 145 80 122 80 103 81 83 95 67 117 58 141 54 151 53 177 55 195 66 207 82 211 94 211 116 204 139 189 149 171 152
Polygon -7500403 true true 151 54 119 59 96 60 81 50 78 39 87 25 103 18 115 23 121 13 150 1 180 14 189 23 197 17 210 19 222 30 222 44 212 57 192 58
Polygon -16777216 true false 70 185 74 171 223 172 224 186
Polygon -16777216 true false 67 211 71 226 224 226 225 211 67 211
Polygon -16777216 true false 91 257 106 269 195 269 211 255
Line -1 false 144 100 70 87
Line -1 false 70 87 45 87
Line -1 false 45 86 26 97
Line -1 false 26 96 22 115
Line -1 false 22 115 25 130
Line -1 false 26 131 37 141
Line -1 false 37 141 55 144
Line -1 false 55 143 143 101
Line -1 false 141 100 227 138
Line -1 false 227 138 241 137
Line -1 false 241 137 249 129
Line -1 false 249 129 254 110
Line -1 false 253 108 248 97
Line -1 false 249 95 235 82
Line -1 false 235 82 144 100
Polygon -7500403 true true 15 15 300 15 300 0 0 0 0 15 15 15
Rectangle -7500403 true true 285 15 300 300
Rectangle -7500403 true true 0 0 15 300
Rectangle -7500403 true true 0 285 285 300
Rectangle -7500403 true true 45 0 60 285
Rectangle -7500403 true true 90 15 105 285
Rectangle -7500403 true true 150 15 165 285
Rectangle -7500403 true true 195 15 210 300
Rectangle -7500403 true true 240 15 255 285

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

exclamation
false
0
Circle -1 true false 129 215 42
Polygon -1 true false 179 54 179 69 164 189 134 189 119 69 119 54
Circle -1 true false 134 173 30
Circle -1 true false 119 29 60

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

ghost
false
0
Polygon -7500403 true true 30 165 13 164 -2 149 0 135 -2 119 0 105 15 75 30 75 58 104 43 119 43 134 58 134 73 134 88 104 73 44 78 14 103 -1 193 -1 223 29 208 89 208 119 238 134 253 119 240 105 238 89 240 75 255 60 270 60 283 74 300 90 298 104 298 119 300 135 285 135 285 150 268 164 238 179 208 164 208 194 238 209 253 224 268 239 268 269 238 299 178 299 148 284 103 269 58 284 43 299 58 269 103 254 148 254 193 254 163 239 118 209 88 179 73 179 58 164
Line -16777216 false 189 253 215 253
Circle -16777216 true false 102 30 30
Polygon -16777216 true false 165 105 135 105 120 120 105 105 135 75 165 75 195 105 180 120
Circle -16777216 true false 160 30 30

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

skeleton1
false
0
Polygon -7500403 true true 225 135 255 150 60 255 45 225
Polygon -7500403 true true 225 255 240 225 75 135 60 150
Circle -7500403 true true 89 74 124
Rectangle -7500403 true true 105 165 195 210

skull
false
0
Polygon -7500403 true true 126 210 111 150 201 150 186 210 126 210
Circle -7500403 true true 103 100 106
Polygon -7500403 true true 67 162 220 235 227 215 77 145 66 162 66 162
Circle -7500403 true true 126 178 60
Circle -7500403 true true 222 145 17
Circle -7500403 true true 78 222 17
Circle -7500403 true true 225 154 17
Circle -7500403 true true 72 212 17
Circle -7500403 true true 64 149 17
Circle -7500403 true true 68 141 17
Circle -7500403 true true 213 220 17
Circle -7500403 true true 216 212 17
Polygon -7500403 true true 75 220 228 147 235 167 85 237 74 220 74 220

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.4.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="Reintrod-DiffReprRate" repetitions="10" runMetricsEveryStep="true">
    <setup>startup
setup-reintroduction
set cull-bears? TRUE</setup>
    <go>go</go>
    <timeLimit steps="8040"/>
    <exitCondition>year = 2024 or not any? bears</exitCondition>
    <metric>count bears</metric>
    <metric>poach-count</metric>
    <metric>cull-count</metric>
    <steppedValueSet variable="initial-bear-reproduction-rate" first="0.1" step="0.1" last="1"/>
  </experiment>
  <experiment name="Reintrod-DiffReprRate_updated" repetitions="20" runMetricsEveryStep="false">
    <setup>startup
setup-reintroduction
set cull-bears? TRUE
set educate? TRUE</setup>
    <go>go</go>
    <exitCondition>(year = 2024 and month = 6) or not any? bears</exitCondition>
    <metric>count bears</metric>
    <metric>mean [confidence] of bears</metric>
    <metric>bear-reproduction-rate</metric>
    <metric>encounter-count</metric>
    <metric>attack-count</metric>
    <metric>cull-count</metric>
    <metric>poach-count</metric>
    <steppedValueSet variable="initial-bear-reproduction-rate" first="0.1" step="0.2" last="1"/>
  </experiment>
  <experiment name="2002-2024_ValidationRetrospective" repetitions="100" runMetricsEveryStep="false">
    <setup>startup
setup-reintroduction
set cull-bears? TRUE
set educate? TRUE</setup>
    <go>go</go>
    <exitCondition>year = 2024  or not any? bears</exitCondition>
    <metric>count bears</metric>
    <metric>count bears with [gender = "male"]</metric>
    <metric>count bears with [gender = "female"]</metric>
    <metric>mean [age] of bears</metric>
    <metric>count bears with [flagged?]</metric>
    <metric>mean [confidence] of bears</metric>
    <metric>bear-reproduction-rate</metric>
    <metric>encounter-count</metric>
    <metric>attack-count</metric>
    <metric>cull-count</metric>
    <metric>poach-count</metric>
    <metric>fear</metric>
    <metric>dissatisfaction</metric>
  </experiment>
  <experiment name="2024-2050_ExplorativeFuture" repetitions="50" runMetricsEveryStep="false">
    <setup>startup
setup
ask beehives [ 
  set fenced? TRUE
  set shape "bee-fenced"
  ]</setup>
    <go>go</go>
    <postRun>;export-plot "Fear over time" "/Users/saramarini/Downloads/2024-2050_FearOverTime.csv"
;export-plot "Bears: total (black) and flagged (red)" "/Users/saramarini/Downloads/2024-2050_EvolutionBearPopulation.csv"

ask turtles with [ breed != attack-spot-markers and 
                   breed != poach-spot-markers] [
                   die
                   ]
ask patches [ set plabel ""]

let run-id (word "run" behaviorspace-run-number)
export-view (word "/Users/saramarini/NetLogo/netlogo-brown-bears/Bears_BehaviorSpace/2024-2050_Views_trial/2024-2050_View-" run-id ".png")</postRun>
    <exitCondition>year = 2050  or not any? bears</exitCondition>
    <metric>count bears</metric>
    <metric>count bears with [gender = "male"]</metric>
    <metric>count bears with [gender = "female"]</metric>
    <metric>mean [age] of bears</metric>
    <metric>count bears with [flagged?]</metric>
    <metric>count bears with [injured?]</metric>
    <metric>mean [confidence] of bears</metric>
    <metric>bear-reproduction-rate</metric>
    <metric>encounter-count</metric>
    <metric>attack-count</metric>
    <metric>cull-count</metric>
    <metric>poach-count</metric>
    <metric>fear</metric>
    <metric>dissatisfaction</metric>
    <metric>bear-education-count</metric>
    <enumeratedValueSet variable="cull-bears?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="educate?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-bears">
      <value value="87"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-bear-reproduction-rate">
      <value value="0.91"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-humans">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="initial-fear">
      <value value="9"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
