;;--------------------------------------------------------------------------------
;;                      Attack dynamic
;;--------------------------------------------------------------------------------

to run-attack-dynamic
  ask bears [
    if any? humans-here [
      let target-human one-of humans-here
      set encounter-count encounter-count + 1
      set fear fear + 1
      calculate-attack-with target-human
    ]
  ]
end

; Calculate the probability of an attack happening depending on the encounter characteristics (person's & bear's features)
to calculate-attack-with [target-human]
  let random-factor (random-float 1) - 0.5
  set attack-risk-score
      (ifelse-value [in-group?] of target-human [0] [1])        ; Convert human-specific characteristics to numbers
    + (ifelse-value [with-dog?] of target-human [2] [0])        ; and subtract bear-education value (the more calmly
    - bear-education                                            ; the human reacts, the less likely the bear attacks)
    
    + (ifelse-value [has-cubs?] of self [4] [0])                ; Convert bear-specific variables to numbers
    + (ifelse-value [injured?] of self [2] [0])
    + (ifelse-value [food?] of patch-here [2] [0])              ; Previous attacks to people or animals (flagged? = TRUE)
    + (ifelse-value [flagged?] of self [2.5] [0])               ; used as proxy of innate aggressiveness of the bear                                                          
    
    + random-factor                                             ; To account for randomness of reality: bear could attack in apparently
                                                                ; low-risk situations and not attack even if all risk factors are ticked
    
  if attack-risk-score > 5 [                                    ; Number empirically selected in such a way as to have a 
                                                                ; plausible number of attacks in the time frame 2002 - 2024
    set attack-count attack-count + 1                            
    set fear fear + 3                                           
    
    ifelse cull-bears? [
      set dissatisfaction dissatisfaction + 1
      ifelse flagged? [
        die
      ] [
        set flagged? TRUE
      ]
    ] [
      set dissatisfaction dissatisfaction + 2
    ]  
    
    set attack-happened? TRUE                                   ; globals needed for the attack animation
    set attack-spot patch-here
    set last-attack-tick ticks

    ask attack-spot [
      set plabel "Attack spot"
      set attack-tick-spot ticks
    ]

    ask humans [                                                ; when attack happens most humans go back to village
      ifelse random 1 < 0.8 [
        move-to one-of patches with [ land-use  = "settlement" ]
      ] [
        move-humans
      ]
    ]
  ]
end