# NetLogo Brown Bears

## Description
The model was coded as an examination element for the class 'Ecosystem Modeling' taught by Dr. Carsten Lemmen at Leuphana University, Summer Term 2024.  
This model simulates human-bear interactions around the settlemet of Caldes, Trentino, Italy. The region (Trentino) was the heart of a rewildering project completed in 2002 and oftentimes the scenario of conflicts between local inhabitants and the big carnivore. In the spring of 2023, one bear-human encounter in the woods around Caldes resulted in a lethal attack and the topic of human-bear coexistence strongly returned at the center of political and scientific attention.  
This model focuses exactly on the dynamics of interaction and interference between the brown bear population and human activities. It aims to understand the population dynamics and the chances of attacks happening in the region taking into consideration human factors (e.g. number of locals, their knowledge about bears), characteristics of brown bears in the region (e.g. population size, their increasing confidence in approaching settlements), as well as different management approaches to improve the coexistence of the two species (e.g. culling, promoting education about bears).
 
## Authors and acknowledgment
This model is authored by Sara Marini and Mick Neumann.

## License
Copyright 2024 CC-BY 4.0 Sara Marini and Mick Neumann.

## Model preview (interface)
![Model Interface](NetLogoBears_pics/NetLogoBears_interface.png)


