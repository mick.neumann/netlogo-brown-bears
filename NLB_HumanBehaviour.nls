;;--------------------------------------------------------------------------------
;;                      Human behaviour
;;--------------------------------------------------------------------------------

to go-humans
  move-humans
  poach
end



;; --------------------- Human movement -------------------------------------------------

; How people move in the model create a balancing feedback loop with fear level and
; frequency of attacks.
; Humans' tendency to choose a road patch depends on fear level. When fear = 0, people
; tend to be on road patches in half of the cases (baseline of 0.5). The higher fear,
; the higher the likelihood that people stick to roads (+ fear / 25). This is turn
; decreases the likelihood of bears meeting humans, thereby also the likelihood of bears
; attacking humans. The lower frequency of attacks lets the general fear level decrease,
; which in turn decreases the value of road tendency. This means that more people tend
; to move outside of roads, which increases the likelihood of human-bear encounters and
; attacks, which increases the general fear level.

; An upper limit of 0.9 exist for road-tendency to account for all the people that still
; leave the roads in spite of fear, e.g. those working in the woods or livestock areas.
; The parameter of 25 has been chosen so that road tendency equals the upper limit of 0.9
; when fear = 10. For fear >= 10 the upper limit of 0.90 is taken as a road tendency value.


to move-humans

 let p-move 0.5                                                                          ; General probability of a human moving to another patch

 ask humans [
   if (random-float 1) <= p-move [
     let rnd random-float 1
     let road-tendency ( min list (0.5 + fear / 25) 0.90 )                               ; Tendency to choose a road patch depends on fear level
     let step (1 - road-tendency) / 4                                                    ; Tendency to choose other land-uses is set through "step", dependent on road-tendency

     if rnd < road-tendency [                                                            ; Depending on rnd and the boundaries between land-uses preferred by humans set
       (move-to-random-patch-by-type "road" 20)                                          ; through "step" 4 cases are identified. The patch to go to identified by each
        ]                                                                                ; human is selected through the procedure "move-to-random-patch-by-type" below
     if rnd >= road-tendency and rnd < road-tendency + step [
       (move-to-random-patch-by-type "settlement" 20)
     ]
     if rnd >= road-tendency + step and rnd < road-tendency + 2 *  step [
       (move-to-random-patch-by-type "livestock" 20)
     ]
     if rnd >= road-tendency + 2 * step [
       (move-to-random-patch-by-type "forest" 20)
     ]
   ]
 ]
end

to move-to-random-patch-by-type [land-type radius]                                       ; Procedure to select patch to go to according to different land-uses
 let patch-to-go-humans one-of ((patches in-radius radius) with [land-use = land-type])

 if patch-to-go-humans != nobody [                                                       ; make sure that at least one patch with the desired land-use exists within that radius
    move-to patch-to-go-humans
 ]
end



;; --------------------- Poaching -------------------------------------------------------
to poach
  let rnd random-float 100
  
  if rnd < 1 and (
    ( count bears > 50 and dissatisfaction > 4 and fear > 6 ) or
    ( count bears > 50 and fear > 8 ) or
    ( count bears > 150 and not cull-bears? ) ) [
    
    ask one-of bears [
      set poach-count poach-count + 1
      set poach-happened? TRUE           ; globals needed to play the poach animation
      set poach-spot patch-here
      ask poach-spot [
        set plabel "Poach"
        set poach-tick ticks
      ]
      if rnd < 0.75 [
        die
        set fear fear - 0.5
      ]
      if rnd > 0.25 [
        set injured? TRUE
        set injured-tick ticks
      ]
    ]
  ]
end



